#!/bin/sh

echo "Download data from Google Bucket"

echo "Lemmatization"
spacy download en_core_web_sm

echo "Running ML training pipeline"
python model/train.py


#echo "$(TZ=GMT-3 date)" >> model/last_ci_time.txt  # $(date "+%Y.%m.%d-%H.%M")
echo "Done"

ls -R .