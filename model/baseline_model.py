from gensim.models import KeyedVectors
from nltk.metrics.distance import edit_distance
from abc import ABC, abstractmethod

import spacy


class AbstractPlayer(ABC):

    @abstractmethod
    def explain(self, word, n_words):
        pass

    @abstractmethod
    def guess(self, word, n_words):
        pass


class LocalPlayer(AbstractPlayer):

    def __init__(self, root, skipgram_explain, spacy2_lemm, ft_skipgram, spacy2):
        print('root', root)
        self.skipgram_explain = KeyedVectors.load_word2vec_format(f'{root}/model/saved_models/{skipgram_explain}',
                                                                  binary=True)
        self.ft_skipgram = KeyedVectors.load_word2vec_format(f'{root}/model/saved_models/{ft_skipgram}',
                                                             binary=True)
        self.spacy2 = KeyedVectors.load_word2vec_format(f'{root}/model/saved_models/{spacy2}',
                                                        binary=True)
        self.spacy2_lemm = KeyedVectors.load(f'{root}/model/saved_models/{spacy2_lemm}',
                                             mmap='r').wv
        self.nlp = spacy.load('en_core_web_sm', disable=['parser', 'ner'])
        self.lemma_tags = {"NNS", "NNPS", "VBZ"}
        self.counter = 0
        self.n_words = 1
        self.n = 2  # n words for combinations in explain methods

    def explain(self, word, n_words):
        l_word = self.nlp(word)[0]
        #
        # if l_word.lemma_ == word:
        #     lst = self.ft_skipgram.most_similar(positive=word, topn=15)
        # else:
        #     # our model skipgram_explain is sensible to plural form.
        #     # in contrary to old_cbow_explain
        #     # if l_word.tag_ in self.lemma_tags:
        #     #     word = l_word.lemma_
        #     word = l_word.lemma_
        #     lst = self.ft_skipgram.most_similar(positive=word, topn=15)
        # res = [w for w, sim in lst]

        if l_word.tag_ in self.lemma_tags:
            word = l_word.lemma_

        model1 = self.skipgram_explain.most_similar(positive=word, topn=9)
        model1 = [w for w, sim in model1]
        model2 = self.spacy2_lemm.most_similar(positive=word, topn=9)
        model2 = [w for w, sim in model2]

        first = model1[:self.n] + model2[:self.n]
        second = model1[self.n:] + model2[self.n:]

        # combine two models
        # order is important
        first_elements = list(dict.fromkeys(first))
        last_elements = list(dict.fromkeys(second))
        res = list(dict.fromkeys(first_elements + last_elements))
        # print('first_elements', first_elements)
        # print('last_elements', last_elements)
        # print('res', res)

        # rest assured we don't duplicate the host word
        new_res = []
        for w in res:
            # don't forget add prob (w, prob) if you shut down the stacking
            if edit_distance(w, word) > 2 and word not in w:
                new_res.append(w)

        return new_res[:n_words]

    def lemmatization(self, model_wv, words, n_words):
        # проверяем что слово есть, лемматизируем и если не нашли - выкидываем
        new_words = []
        for i, w in enumerate(words):
            if w not in model_wv.index_to_key:
                l_word = self.nlp(w)[0].lemma_
                if l_word not in model_wv.index_to_key:
                    continue
                else:
                    print('lemma', w, l_word)
                    new_words.append(l_word)
            else:
                print('without lemma', w)
                new_words.append(w)
        return new_words[:n_words]

    def guess(self, words, n_words):

        print('counter', self.counter, 'n_words', self.n_words)

        if self.counter % 4 == 1:
            l_words = self.lemmatization(self.skipgram_explain, words, self.n_words)
            if l_words:
                res = self.skipgram_explain.most_similar(positive=l_words, topn=15)
            else:
                l_words = self.lemmatization(self.spacy2_lemm, words, self.n_words)
                res = self.spacy2_lemm.most_similar(positive=l_words, topn=15)

        elif self.counter % 4 == 2:
            l_words = self.lemmatization(self.spacy2, words, self.n_words)
            if l_words:
                res = self.spacy2.most_similar(positive=l_words, topn=15)
            else:
                l_words = self.lemmatization(self.spacy2_lemm, words, self.n_words)
                res = self.spacy2_lemm.most_similar(positive=l_words, topn=15)

        elif self.counter % 4 == 3:
            l_words = self.lemmatization(self.ft_skipgram, words, self.n_words)
            if l_words:
                res = self.ft_skipgram.most_similar(positive=l_words, topn=15)
            else:
                self.n_words += 1
                l_words = self.lemmatization(self.skipgram_explain, words, self.n_words)
                res = self.skipgram_explain.most_similar(positive=l_words, topn=15)

        else:
            self.n_words += 1
            l_words = self.lemmatization(self.spacy2_lemm, words, self.n_words)
            if l_words:
                res = self.spacy2_lemm.most_similar(positive=l_words, topn=15)
            else:
                l_words = self.lemmatization(self.skipgram_explain, words, self.n_words)
                res = self.skipgram_explain.most_similar(positive=l_words, topn=15)
        self.counter += 1

        res = [i[0] for i in res if len(i[0]) > 3]
        return res[:n_words]
