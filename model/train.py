import gensim
import os
import spacy

from gensim.models import KeyedVectors
# from gensim.models import FastText

from gensim.parsing.preprocessing import remove_stopwords
from google.cloud import storage
from time import gmtime, strftime


root = os.path.dirname(os.path.abspath(__file__))


def update_data(path):
    os.makedirs(path, exist_ok=True)
    _, _, filenames = next(os.walk(path))
    existed_buckets = [name.replace('.txt', '') for name in filenames]

    client = storage.Client()
    bucket = client.bucket('dmia-mlops-texts')
    blobs = list(bucket.list_blobs())
    new_files = []

    for b in blobs:
        if b.name in existed_buckets:
            print('old_file', b.name)
            continue
        else:
            print('new_file', b.name)
            b.download_to_filename(f'{path}/{b.name}.txt')
            new_files.append(f'{path}/{b.name}.txt')
    return new_files


def read_and_preprocess_data_v1(files):
    nlp = spacy.load('en_core_web_sm', disable=['parser', 'ner'])
    for file in files:
        with open(file, "rb") as f:
            print(strftime("%Y-%m-%d %H:%M:%S", gmtime()), 'docs', file)
            for line in f:
                line = line.decode('utf-8')
                line = remove_stopwords(line.lower())
                line = gensim.utils.simple_preprocess(line)
                tokenized_line = nlp(' '.join(line))

                yield [token.lemma_ for token in tokenized_line]

        # file_size = str(round(os.path.getsize(file) / 10e5, 1)) + ' mb'
        # print('delete file', file, 'size_string', file_size)
        # with open(file, 'w') as f:
        #     f.write(file_size)


def read_and_preprocess_data_v2(files):
    nlp = spacy.load('en_core_web_sm', disable=['parser', 'ner'])
    # correct only plural
    # not necessary for CBOW explain model
    lemma_tags = {"NNS", "NNPS", "VBZ"}
    for file in files:
        with open(file, "rb") as f:
            print(strftime("%Y-%m-%d %H:%M:%S", gmtime()), 'docs', file)
            for line in f:
                line = line.decode('utf-8')
                line = remove_stopwords(line.lower())
                line = gensim.utils.simple_preprocess(line)
                tokenized_line = nlp(' '.join(line))
                yield [token.lemma_ if token.tag_ in lemma_tags else token.text for token in tokenized_line]

        file_size = str(round(os.path.getsize(file) / 10e5, 1)) + ' mb'
        print('delete file', file, 'size_string', file_size)
        with open(file, 'w') as f:
            f.write(file_size)


def update_model(docs, path_to_model):
    model = KeyedVectors.load(path_to_model, mmap='r')
    print('vocab_length_before', len(model.wv.index_to_key), 'docs_length', len(docs))
    model.build_vocab(docs, update=True)
    model.train(docs, total_examples=model.corpus_count, epochs=15)  # 10 #15
    print('vocab_length_after', len(model.wv.index_to_key))
    model.save(path_to_model)
    if 'old_models' in path_to_model:
        model.wv.save_word2vec_format(f'{root}/saved_models/spacy2.bin', binary=True)


def set_environ():
    print('set_environ', f'{root}/resolute_tracer.json')
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = str(f'{root}/data_buckets/resolute_tracer.json')


def main():
    # set_environ()
    new_files = update_data(f'{root}/data_buckets')

    # docs_v1 = list(read_and_preprocess_data_v1(new_files))
    docs_v2 = list(read_and_preprocess_data_v2(new_files))
    # print('len(docs_v1)', len(docs_v1), 'len(docs_v2)', len(docs_v2))
    if docs_v2:
        # print('update spacy_v1')
        # update_model(docs_v1, f'{root}/old_models/spacy2.model')
        print('update spacy_v2')
        update_model(docs_v2, f'{root}/saved_models/spacy2_simple_lemma.model')
    else:
        print('No new documents')


if __name__ == "__main__":
    main()
