from flask import Flask, request, render_template, redirect, url_for
import os
import json
from model.baseline_model import LocalPlayer

app = Flask(__name__)

root = os.path.dirname(os.path.abspath(__file__))
local = LocalPlayer(root, 'skipgram_explain.bin', 'spacy2_simple_lemma.model',
                    'ft_skipgram.bin', 'spacy2.bin')


@app.route('/explain')
def explain():
    word = request.args.get('word')
    n_words = request.args.get('n_words', '')
    if not n_words.isnumeric():
        raise ValueError('Argument "n_words" should be valid number from 1 to 10')
    res = local.explain(word.strip(), int(n_words))
    return json.dumps(res)


@app.route('/guess')
def guess():
    words = request.args.getlist('words')
    n_words = request.args.get('n_words', '')
    if not n_words.isnumeric():
        raise ValueError('Argument "n_words" should be valid number from 1 to 10')
    print('words is HERE', words, 'n_words', n_words)
    res = local.guess(words, int(n_words))
    return json.dumps(res)


@app.route("/", methods=["POST", "GET"])
def index():
    if request.method == 'POST':
        hat_word = request.form.get('hat_word')
        n_words = request.form.get('n_words', '')
        if not n_words.isnumeric():
            raise ValueError('Argument "n_words" should be valid number from 1 to 10')

        print('action', request.form)
        print('root', root)
        if request.form.get('action') == 'explain_word':
            return redirect(url_for('explain', word=hat_word, n_words=n_words))
        if request.form.get('action') == 'guess_word':
            return redirect(url_for('guess', words=hat_word, n_words=n_words))

    return render_template('index.html', password='nothing', prediction=0)


if __name__ == "__main__":
    app.run(host="0.0.0.0", threaded=False, debug=True, port=os.environ.get('PORT', 8080))


